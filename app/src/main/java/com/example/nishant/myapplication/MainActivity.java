package com.example.nishant.myapplication;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
RecyclerView recyclerView;  //Declaration
SearchView searchView;
    MainActivityAdapter adapter;   // Create Class MainActivityAdapter to handle Recycle View
    List<Item> productlists = new ArrayList<>();  //Create class Item to handle cardView Data
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        productlists.add(new Item("apple",  R.drawable.ic_search_black_24dp));
        /* create new instance of class Item pass String for text and int for photo
            in Class Item Initialize constructor of no parameters passing in its instance
          */
        productlists.add(new Item("socks",R.drawable.ic_launcher_foreground));
        productlists.add(new Item("drawing",R.drawable.ic_launcher_foreground));
        productlists.add(new Item("wood",R.drawable.ic_launcher_foreground));

        recyclerView = (RecyclerView)findViewById(R.id.recycleview);
       // recyclerView.hasFixedSize();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);


        adapter = new MainActivityAdapter(productlists,MainActivity.this);  // parametrized Constructor in MainActivityAdapter
        recyclerView.setAdapter(adapter);  //  Extends class RecyclerView.Adapter in MainActivityAdapter class and call override methods in MainActivity adapter such  onCreateViewHolder onBindViewHolder getItemCount
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater(); // This will invoke menu
        menuInflater.inflate(R.menu.search_bar,menu); //this will display menu item on screen by its menu id
        final MenuItem search_bar = menu.findItem(R.id.search_bar); // Initializing search bar
        searchView = (SearchView) search_bar.getActionView();

        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        newText = newText.toLowerCase();   // newText contains user Query
        List<Item>  newlist = new ArrayList<>(); // make object of class Item where data is stored
        String title;
        for(Item name : productlists ){         //Declare Item class object name and loop till end
        title = name.getName().toLowerCase();  //.getName(String) from Item class and store into title
             if(title.contains(newText)){      // if user query matched from dataset update the Item class list
                newlist.add(name);
             }
        }
        adapter.setfilter(newlist);   //Update adapter class method.setFilter i.e in MainActiivityAdapater class
        return true;
    }



} //[ End of class ]
