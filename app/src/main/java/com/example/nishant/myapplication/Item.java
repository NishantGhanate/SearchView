package com.example.nishant.myapplication;

/**
 * Created by Nishant on 12/8/2017.
 */

class Item {

    private String name;
    private int photo;

    public Item(String name, int photo) {
        this.name = name;                       //Using .this Now Class Item will data passed into constructor
        this.photo = photo;
    }
        //Mostly we need above part only

    public String getName() {
        return name;                            //This will return name
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoto() {
        return photo;                           //This will return photo
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }
}
