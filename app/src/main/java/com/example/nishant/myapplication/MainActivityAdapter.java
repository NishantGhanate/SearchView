package com.example.nishant.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nishant on 12/8/2017.
 */

class MainActivityAdapter extends RecyclerView.Adapter<MainActivityAdapter.Holderview> { // .Holderview class will get inflated card layout

    private List<Item> productlist = new ArrayList<>();  //Create list from Item class
    private Context context;


    public MainActivityAdapter(List<Item> productlist, Context context) {
        this.productlist = productlist;
        this.context = context;
    }

    @Override
    public MainActivityAdapter.Holderview onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout= LayoutInflater.from(parent.getContext()).
                inflate(R.layout.card_view,parent,false);

        return new Holderview(layout);

    }

    @Override
    public void onBindViewHolder(MainActivityAdapter.Holderview holder, int position) {
        holder.c_name.setText(productlist.get(position).getName());
        holder.c_image.setImageResource(productlist.get(position).getPhoto());
    }

    @Override
    public int getItemCount() {
        return productlist.size();
    }


    public void setfilter(List<Item> listitem)
    {
        productlist=new ArrayList<>();
        productlist.addAll(listitem);
        notifyDataSetChanged();
    }

    public class Holderview  extends RecyclerView.ViewHolder{
        ImageView c_image;
        TextView c_name;

        public Holderview(View itemView) {
            super(itemView);
            c_image=(ImageView) itemView.findViewById(R.id.product_image);
            c_name = (TextView) itemView.findViewById(R.id.product_title);

        }
    }
}


